.POSIX:
EMACS = emacs

SRC = itchy.el
ELC = itchy.elc

compile: $(ELC)

itchy.elc: $(SRC)

clean:
	@rm -f $(ELC)

.SUFFIXES: .el .elc
.el.elc:
	@$(EMACS) -batch -Q -L . -f batch-byte-compile $<
