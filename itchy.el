;;; itchy.el --- Neat scratch buffer handler -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Peter Hoeg
;;
;; Author: Peter Hoeg <peter@hoeg.com>
;; Maintainer: Peter Hoeg <peter@hoeg.com>
;; Created: May 09, 2024
;; Modified: July 13, 2024
;; Keywords: tools
;; Homepage: https://gitlab.com/peterhoeg/itchy.el
;; SPDX-License-Identifier: GPL-3.0-or-later
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; Helpers for creating scratch buffers.
;;
;;; Code:

(require 'f)

(defcustom itchy-save-directory (file-name-as-directory (expand-file-name "emacs/scratch" (or (getenv "XDG_CACHE_HOME") "~/.cache")))
  "Directory in which to create persistent scratch buffers."
  :type 'string
  :group 'itchy)

(defcustom itchy-default-mode 'elisp
  "Default mode unless otherwise specified."
  :type 'symbol
  :group 'itchy)

(defcustom itchy-new-buffer-fn nil
  "Function to use for creating a new buffer. Will be called with the name of the file."
  :type 'function
  :group 'itchy)

(defvar itchy--mappings '(crystal (:mode crystal-mode :ext "cr" :contents "#!/usr/bin/env crystal")
                          css (:mode css-ts-mode :ext "css")
                          elisp (:mode emacs-lisp-mode :ext "el" :contents ";; -*- lexical-binding: t; -*-")
                          haskell (:mode haskell-mode :ext "hs")
                          html (:mode html-ts-mode :ext "html")
                          js (:mode js-ts-mode :ext "js")
                          json (:mode json-ts-mode :ext "json")
                          markdown (:mode markdown-ts-mode :ext "md")
                          nix (:mode nix-ts-mode :ext "nix")
                          org (:mode org-mode :ext "org")
                          ruby (:mode ruby-ts-mode :ext "rb" :contents "#!/usr/bin/env ruby")
                          rust (:mode rust-ts-mode :ext "rs")
                          scss (:mode scss-mode :ext "scss")
                          shell (:mode bash-ts-mode :ext "sh" :contents "#!/usr/bin/env bash")
                          text (:mode text-mode :ext "txt")
                          toml (:mode toml-ts-mode :ext "toml")
                          yaml (:mode yaml-ts-mode :ext "yml"))
  "Mappings.")

(defun itchy--file-name (ext)
  "Provide the file name for EXT."
  (let* ((name "scratch")
         (base (concat name "." ext))
         (file (if (file-locked-p (expand-file-name base itchy-save-directory))
                   (if (get-buffer base)
                       base
                     (f-filename (make-temp-file (concat name "_") nil (concat "." ext))))
                 base)))
    (expand-file-name file itchy-save-directory)))

;;;###autoload
(defun itchy-open-default ()
  "Open a default scratch buffer."
  (interactive)
  (itchy-open-buffer itchy-default-mode))

;;;###autoload
(defun itchy-register-kind (kind mode extension)
  "Register mappings for KIND, MODE and EXTENSION."
  (setq itchy--mappings (plist-put itchy--mappings kind (list mode extension))))

;;;###autoload
(defun itchy-open-buffer (kind)
  "Open a buffer of the appropriate KIND."
  (when-let* ((entry (plist-get itchy--mappings kind))
              (mode (plist-get entry :mode))
              (ext (plist-get entry :ext))
              (contents (or (plist-get entry :contents) ""))
              (file (expand-file-name (itchy--file-name ext) itchy-save-directory))
              (create-lockfiles 't))
    (unless (file-exists-p file)
      (make-empty-file file 'parents))
    ;; (lock-file file)
    (when (fboundp itchy-new-buffer-fn)
      (funcall itchy-new-buffer-fn file))
    (find-file file)
    (when (< (buffer-size) (length contents))
      (save-excursion
        (goto-char (point-min))
        (insert contents ?\n)))))

(provide 'itchy)

;;; itchy.el ends here
